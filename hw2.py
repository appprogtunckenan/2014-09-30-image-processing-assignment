#Beginning work with scipy.ndimage:
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import scipy.ndimage as ndimage
import scipy.misc as misc
from glob import glob
import random
from random import randint

# Set the size of the image
imsize = 256

#Image dpi value
my_dpi = 1

#image angular velocity set as degree per frame
ang_vel = 1
#Minimum and maximum locations for vertices
#Chosen such that the triangle does not hit 
#the edges while rotating
min_loc = 20
max_loc =90

#Create empty image
the_im = np.zeros((imsize,imsize))
# create array for vertices
p = np.zeros((1,6))


def ds_vertice_generator(min_loc,max_loc,imsize):

	#Setting corner points
	p_0 = np.array([randint(-max_loc,-min_loc)+imsize/2,imsize/2])
	p_2 = np.array([randint(min_loc,max_loc)+imsize/2,-randint(min_loc,max_loc)+imsize/2])
	p_1 = np.array([randint(min_loc,max_loc)+imsize/2,randint(min_loc,max_loc)+imsize/2] )

	#fill the points into an array
	p = [p_0,p_1,p_2]

	#Setting edges as vectors (this portion is not used in the program)
	v_0 = p_2-p_0
	v_1 = p_1-p_2
	v_2 = p_2-p_1

	v = [v_0,v_1,v_2]

	return p



def ds_draw_triangle(p,my_dpi):

	#This is a check variable for point location (inside or outside of the triangle)
	count=0

	for x_im in xrange(0,imsize-1):
		for y_im in xrange(0,imsize-1):

			#Get points in image
			p_get = np.array([x_im,y_im]) 

			#Check the point is inside the vertices or not
			for n in range(0,3):
				
				#Define vectors from each corner point
				#Notice that these vectors are picked according to the right hand rule
				#positive rotation is towards the inside of the triangle
				v1 = p_get-p[n-1]
				v2 = p[n]-p[n-1]

				#if cross product of the vectors are less than zero
				#for any of these vector sets than the point is outside the triangle
				if np.cross(v1,v2) <= 0: 
					count += 1

			#Point is inside the triangle
			if count == 0:
				the_im[x_im, y_im] = 1
					
			count=0

	#Plot triangle
	plt.figure(figsize=(imsize, imsize),dpi=my_dpi)
	plt.imshow(the_im, cmap=plt.cm.gray, interpolation='none')
	plt.axis('off')
	plt.show()

	return the_im

def ds_triangle_animation_and_save(the_im,my_dpi,ang_vel):

	#initialize figure for animation
	plt.figure(figsize=(imsize, imsize),dpi=my_dpi)
	#hold to 
	plt.hold


	for ang in xrange(0,180):

		#Built in rotate function from ndimage
		#A higher order is chosen to prevent image from getting blurry
		#Reshape is set to false to avoid the image size change
		the_im = ndimage.rotate(the_im, 1, axes=(0,0), reshape=False, output=None, order=5, cval=0.0, prefilter=True)

		#interactive plot on
		plt.ion()
		#display the current image
		plt.imshow(the_im, cmap=plt.cm.gray, interpolation='none')
		plt.axis('off')
		fig = plt.gcf()
		plt.show()
		# required for updating
		plt.pause(0.00001)
	    #save the plot into the folder as .jpeg
		fig.savefig('rotate(%d).jpeg' % ang,cmap=plt.cm.gray,dpi=my_dpi)
		#clear the current plot
		plt.clf()

#set vertices
p = ds_vertice_generator(min_loc,max_loc,imsize)
#Get initial triangle image
the_im =ds_draw_triangle(p,my_dpi)
#Animate for 180 degrees and save
ds_triangle_animation_and_save(the_im,my_dpi,ang_vel)