import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as col
import matplotlib.image as mpimg
import scipy.ndimage as ndimage
import scipy.misc as misc
from glob import glob
import random
from random import randint
from scipy import linalg

#Part 2 Harris detector and post processing

# Set the size of the image
imsize = 256

#maximum frame number
max_frame_no = 179
# Get two sample images for part 3

#Get a frame to process from saved images
#pick a random frame
frame1 = randint(0,max_frame_no)
#pick the second frame wrt to first one with a random amount of rotation
a1 = (frame1+1)%max_frame_no
a2 = (frame1+5)%max_frame_no
if a2>a1:
	ang_change = randint(a1,a2)
else:
	ang_change = randint(a2,a1)

frame2= (frame1+ang_change)%max_frame_no

#read .jpeg in gray scale
img =misc.imread('rotate(%d).jpeg' % frame1,flatten=True)
#Set threshold for the binary conversion
threshold = 100
#Parameters for Harris filter
#Gaussian window size
winsize = 9
#sigma value for normal distribution (There are two different sigmas 
#because different sigma values gives better results depending on purpose (edge or corner))
sigma_cor = 2.5
sigma_edge = 0.5
#Constant for tracemultiplier for R value
k = 0.06
#spacing of harris filter (how)
space = 1

#Set R value thresholds for corner and edge
t_cor = 3
t_edge = -0.5
#window size for edges and corners
#to see them more clear in images
winsize_point =1

def ds_Jpeg2binary(img,threshold):

	for x_im in xrange(0,img.shape[0]):
		for y_im in xrange(0,img.shape[1]):

			if img[x_im,y_im]>=threshold:
				img[x_im,y_im] = 1
			else:
				img[x_im,y_im] = 0

	# plt.figure()
	# plt.hold
	# plt.imshow(img,cmap=plt.cm.gray, interpolation='none')
	# plt.show()

	return img

def ds_sobel_filt(img):
	Ix = ndimage.sobel(img, axis=0)
	Iy = ndimage.sobel(img, axis=1)

	#Get directional gradients for Harris detector
	Ix_2 = np.multiply(Ix,Ix)
	Iy_2 = np.multiply(Iy,Iy)
	Ixy = np.multiply(Ix,Iy)

	#Get directional derivatives
	return Ix_2,Iy_2,Ixy

def ds_create_gw(winsize,sigma):

	#set window for gaussian
	gw = np.zeros((winsize,winsize))

	#Set windows for directional derivatives
	Ix2_window = np.zeros((winsize,winsize))
	Iy2_window = np.zeros((winsize,winsize))
	Ixy_window = np.zeros((winsize,winsize))

	#Get the center value for the window.
	center = (winsize-1)/2

	#Create gaussian window
	gw[center,center] = 1
	gw=ndimage.gaussian_filter(gw,sigma)

	return gw,Ix2_window,Iy2_window,Ixy_window,center





def ds_Harris(img,gw,Ix2_window,Iy2_window,Ixy_window,k,space,t_cor,t_edge,winsize_point,center):

	#Initialize Harris images
	Harris_Im = np.zeros(img.shape)
	Harris_cor = np.zeros(img.shape)
	Harris_edge = np.zeros(img.shape)


	for x_im in xrange(center,img.shape[0]-center,space):
		for y_im in xrange(center,img.shape[1]-center,space):


			#Get windows
			Ix2_window = Ix_2[x_im-center:x_im-center+winsize,y_im-center:y_im-center+winsize]
			Iy2_window = Iy_2[x_im-center:x_im-center+winsize,y_im-center:y_im-center+winsize]
			Ixy_window = Ixy[x_im-center:x_im-center+winsize,y_im-center:y_im-center+winsize]

			#Multiply with gaussian window
			SIx2 = np.sum(np.multiply(Ix2_window, gw))
			SIy2 = np.sum(np.multiply(Iy2_window, gw))
			SIxy = np.sum(np.multiply(Ixy_window, gw))

			#Get the M matrix in Harris detectors eq
			M = np.array([[SIx2,SIxy],[SIxy,SIy2]])

			#Get the determinant
			det_M = linalg.det(M)

			#Get the trace value
			trc_M = np.trace(M)

			#R values for detection (Main equation of harris detector)
			R = det_M - k*(np.multiply(trc_M,trc_M)) 

			#Detecting corners and create a corner only image
			if R>t_cor:
			  	Harris_cor[x_im-center:x_im-center+winsize_point,y_im-center:y_im-center+winsize_point] = 100
			else:
				Harris_cor[x_im, y_im] = 0

			#Detecting edges and create a edge only image
			if R<t_edge:
			  	Harris_edge[x_im-center:x_im-center+winsize_point,y_im-center:y_im-center+winsize_point] = 100
			else:
				Harris_edge[x_im, y_im] = 0

			#Create original image
			Harris_Im[x_im, y_im] = R

	return Harris_Im,Harris_edge,Harris_cor

def ds_plot_HI (Harris_Im):
	plt.figure()
	plt.hold
	plt.imshow(Harris_Im,cmap=plt.cm.gray, interpolation='none')
	plt.show()

def ds_plot_HI_cor (Harris_cor):
	plt.figure()
	plt.hold
	plt.imshow(Harris_cor,cmap=plt.cm.gray, interpolation='none')
	plt.show()

def ds_plot_HI_edge (Harris_edge):
	plt.figure()
	plt.hold
	plt.imshow(Harris_edge,cmap=plt.cm.gray, interpolation='none')
	plt.show()

#Get binary image
img_binary = ds_Jpeg2binary(img,threshold)
#Get directional derivatives for Harris
[Ix_2,Iy_2,Ixy]=ds_sobel_filt(img_binary)
#Get gaussian window
[gw_cor,Ix2_window,Iy2_window,Ixy_window,center] = ds_create_gw(winsize,sigma_cor)
[gw_edge,Ix2_window,Iy2_window,Ixy_window,center] = ds_create_gw(winsize,sigma_edge)
#Get Harris images (corner)
[Harris_Im,Harris_edge_bad,Harris_cor] = ds_Harris(img_binary,gw_cor,Ix2_window,Iy2_window,Ixy_window,k,space,t_cor,t_edge,winsize_point,center)
#Get Harris images (edge)
[Harris_Im,Harris_edge,Harris_cor_bad] = ds_Harris(img_binary,gw_edge,Ix2_window,Iy2_window,Ixy_window,k,space,t_cor,t_edge,winsize_point,center)

#plot originalimage
ds_plot_HI (Harris_Im)
#plot corners
ds_plot_HI_cor (Harris_cor)
#plot edges
ds_plot_HI_edge (Harris_edge)


#Part 3 simple velocity field and vertice velocity approximation 

#from part 2 use detected corner clusters from Harris detector
#This portion is time consuming a faster detector will be more suitable for
#real-time data

max_angle = 120
angular_rate = 2
offset = 60


def ds_Harris_extract_corner_points(offset,max_angle,angular_rate,threshold):

	#initialize velocity lists
	v_1x=[]
	v_2x=[]
	v_3x=[]
	v_1y=[]
	v_2y=[]
	v_3y=[]
	#Index for harris images
	h_index = 0
	for x in range(0,max_angle-2,angular_rate):

		im_i = misc.imread('corner_rotate(%d).jpeg' % x,flatten=True)
		im_i = ds_Jpeg2binary(im_i,threshold)
		next_img_index = x+angular_rate 
		im_f = misc.imread('corner_rotate(%d).jpeg' % next_img_index,flatten=True)
		im_f = ds_Jpeg2binary(im_f,threshold)

		#im_i: initial or current frame , im_f: final or next frame
		#Crop the additional corners coming from imread function
		im_i = im_i[offset:im_i.shape[0]-offset,offset:im_i.shape[1]-offset]
		im_f = im_f[offset:im_f.shape[0]-offset,offset:im_f.shape[1]-offset]

		#Label corner clusters
		im_i_labeled,features_no_1 = ndimage.label(im_i)
		im_f_labeled,features_no_2 = ndimage.label(im_f)

		#Separate point clusters
		im_i_p1_cluster = np.equal(im_i_labeled,1)
		im_i_p2_cluster = np.equal(im_i_labeled,2)
		im_i_p3_cluster = np.equal(im_i_labeled,3)

		im_f_p1_cluster = np.equal(im_f_labeled,1)
		im_f_p2_cluster = np.equal(im_f_labeled,2)
		im_f_p3_cluster = np.equal(im_f_labeled,3)


		#reduce clusters to single points by taking their center of mass
		im_i_p1 = ndimage.measurements.center_of_mass(im_i_p1_cluster)
		im_i_p2 = ndimage.measurements.center_of_mass(im_i_p2_cluster)
		im_i_p3 = ndimage.measurements.center_of_mass(im_i_p3_cluster)

		im_f_p1 = ndimage.measurements.center_of_mass(im_f_p1_cluster)
		im_f_p2 = ndimage.measurements.center_of_mass(im_f_p2_cluster)
		im_f_p3 = ndimage.measurements.center_of_mass(im_f_p3_cluster)

		#Get velocity vector for each corner
		v_1x.append(im_f_p1[0]-im_i_p1[0])
		v_1y.append(im_f_p1[1]-im_i_p1[1])
		v_2x.append(im_f_p2[0]-im_i_p2[0])
		v_2y.append(im_f_p2[1]-im_i_p2[1])
		v_3x.append(im_f_p3[0]-im_i_p3[0])
		v_3y.append(im_f_p3[1]-im_i_p3[1])

		h_index+=1
		if h_index >= angular_rate-1:
			h_index = angular_rate-1 
		im_i = []
		im_f = []


	return v_1x,v_2x,v_3x,v_1y,v_2y,v_3y

#Plot corner velocities
def ds_plot_corner_vel (v1x,v2x,v3x,v1y,v2y,v3y):

	plt.figure()
	plt.subplot(131)
	plt.ylabel('Vy[pixel/frame]')
	plt.plot(v1y)
	plt.xlabel('C1')
	plt.subplot(132)
	plt.plot(v2y)
	plt.xlabel('C2')
	plt.title('Corner Velocities Vertical')
	plt.subplot(133)
	plt.plot(v3y)
	plt.xlabel('C3')

	fig = plt.gcf()
	fig.savefig('Vy_corners')

	plt.figure()
	plt.subplot(131)
	plt.ylabel('Vx[pixel/frame]')
	plt.plot(v1x)
	plt.xlabel('C1')
	plt.subplot(132)
	plt.plot(v2x)
	plt.xlabel('C2')
	plt.title('Corner Velocities Horizontal')
	plt.subplot(133)
	plt.plot(v3x)
	plt.xlabel('C3')
	fig = plt.gcf()
	fig.savefig('Vx_corners')

#Animates corner cluster vs actual image
def final_animation(max_angle,angular_rate):
	plt.figure()
	plt.hold
	h_rate = 0
	for x in range(0,max_angle,angular_rate):
		img =misc.imread('rotate(%d).jpeg' % x,flatten=True)
		H_im =misc.imread('corner_rotate(%d).jpeg' % x,flatten=True)
		plt.ion()
		#display the current image
		plt.subplot(121)
		plt.imshow(img, cmap=plt.cm.gray, interpolation='none')
		plt.subplot(122)
		plt.imshow(H_im, cmap=plt.cm.gray, interpolation='none')

		plt.axis('off')

		fig = plt.gcf()
		plt.show()
		# required for updating
		plt.pause(0.00001)
		#clear the current plot
		plt.clf()

		h_rate +=1

# This portion of the code is used to collect and save consecutive frames processed by Harris filter
# It can be commented out however the process is time consuming 5-10 min

# plt.figure(figsize=(imsize, imsize),dpi=1)
# plt.hold
# for x in range(0,max_angle,angular_rate):

# 	img =misc.imread('rotate(%d).jpeg' % x,flatten=True)

# 	#Get binary image
# 	img_binary = ds_Jpeg2binary(img,threshold)
# 	#Get directional derivatives for Harris
# 	[Ix_2,Iy_2,Ixy]=ds_sobel_filt(img_binary)
# 	#Get gaussian window
# 	[gw_cor,Ix2_window,Iy2_window,Ixy_window,center] = ds_create_gw(winsize,sigma_cor)
# 	#Get Harris images (corner)
# 	[Harris_Im,Harris_edge_bad,Harris_cor] = ds_Harris(img_binary,gw_cor,Ix2_window,Iy2_window,Ixy_window,k,space,t_cor,t_edge,winsize_point,center)

# 	plt.ion()
# 	#display the current image
# 	plt.imshow(Harris_cor, cmap=plt.cm.gray, interpolation='none')
# 	plt.axis('off')

# 	fig = plt.gcf()
# 	plt.show()
# 	fig.savefig('corner_rotate(%d).jpeg' % x,cmap=plt.cm.gray,dpi=1)
# 	# required for updating
# 	plt.pause(0.00001)
# 	#clear the current plot
# 	plt.clf()

# 	print 'frame processed ...'

#Get the x and y velocity of each corner
[v1x,v2x,v3x,v1y,v2y,v3y] = ds_Harris_extract_corner_points(offset,max_angle,angular_rate,threshold)
ds_plot_corner_vel(v1x,v2x,v3x,v1y,v2y,v3y)
final_animation(max_angle,angular_rate)




